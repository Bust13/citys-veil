using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
//-4.48 -1.91
public class PlayerControls : MonoBehaviour
{
    public Rigidbody2D rb;
    SpriteRenderer sr;
    public Animator anim;
    private Acciones controles; 


    public float horizontal;
    public float vertical;
    private float speedVertical=2f;
    private float speedHorizontal = 4f;

    private bool miraDerecha = true;
    static Vector2 LimitsY = new Vector2(-7.52f, -8.92f);
    static float LimitXLeft = -9.65f;
    public InputAction attackAction;

    private void Awake()
    {
        controles = new();
    }
    private void OnEnable()
    {
        controles.Enable();
        controles.Pegar.Golpe.started += Attack;

    }
    private void OnDisable()
    {
        controles.Disable();
    }
    private void Attack(InputAction.CallbackContext context)
    {
        anim.SetTrigger("Golpeando");
        Debug.Log("Esta Golpeando");
    }
    void Update()
    {
        rb.velocity = new Vector2(horizontal* speedHorizontal, vertical* speedVertical);
        if (!miraDerecha&& horizontal > 0f)
        {
            Flip();
            Debug.Log("Mira Derecha");
        }else if(miraDerecha&& horizontal < 0f)
        {
            Flip();
            Debug.Log("Mira Izquierda");
        }
        float clampedX = Mathf.Clamp(transform.position.x, LimitXLeft, float.MaxValue);
        transform.position = new Vector3(clampedX, transform.position.y, transform.position.z);
        transform.position= new Vector3(transform.position.x, Mathf.Clamp(transform.position.y,LimitsY.y,LimitsY.x),transform.position.z );
        if(horizontal != 0f || vertical != 0f)
        {
            anim.SetBool("EstaCaminando",true);
        }
        else
        {
            anim.SetBool("EstaCaminando", false);
        }
        
        
    }
    private void Flip()
    {
        miraDerecha= !miraDerecha;
        Vector3 localScale= transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;
    }
    public void Move(InputAction.CallbackContext context)
    {
       Vector2 input = context.ReadValue<Vector2>();
       horizontal= input.x;
       vertical= input.y;
    }
    
}
