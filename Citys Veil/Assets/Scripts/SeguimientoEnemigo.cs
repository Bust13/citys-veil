using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguimientoEnemigo : MonoBehaviour
{
    public Transform target; // El transform del jugador al que seguir� el enemigo
    public float moveSpeed = 3f; // Velocidad de movimiento del enemigo
    public float detectionRange = 5f; // Rango de detecci�n del enemigo
    public float attackRange = 1f; // Rango de ataque del enemigo
    public LayerMask playerLayer; // Capa del jugador

    private Rigidbody2D rb;
    private Animator animator;
    private bool miraDerecha; // Variable para controlar la direcci�n en que mira el enemigo
    private bool isAttacking; // Variable para controlar si el enemigo est� atacando

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>(); // Obtenemos el componente Animator

        // Inicializaci�n de la direcci�n del enemigo para que mire hacia el jugador al iniciar
        if (target != null)
        {
            miraDerecha = target.position.x > transform.position.x;
            if (!miraDerecha)
            {
                Flip();
            }
        }
    }

    void Update()
    {
        if (target != null)
        {
            // Calcula la distancia al objetivo
            float distanceToTarget = Vector2.Distance(transform.position, target.position);

            // Comprueba si el jugador est� dentro del rango de detecci�n
            if (distanceToTarget <= detectionRange)
            {
                // Si el jugador est� dentro del rango de ataque
                if (distanceToTarget <= attackRange)
                {
                    // Detenemos el movimiento del enemigo y activamos la animaci�n de ataque
                    rb.velocity = Vector2.zero;
                    animator.SetBool("Camina", false);

                    if (!isAttacking)
                    {
                        animator.SetTrigger("Ataque");
                        isAttacking = true;
                    }
                }
                else
                {
                    // Si el jugador est� fuera del rango de ataque, pero dentro del rango de detecci�n, el enemigo deber�a caminar hacia �l
                    Vector2 directionToPlayer = ((Vector2)target.position - (Vector2)transform.position).normalized;

                    // Lanza un Raycast desde el centro del enemigo en la direcci�n del jugador
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, directionToPlayer, detectionRange, playerLayer);

                    if (hit.collider != null && hit.collider.CompareTag("Player"))
                    {
                        // Si el Raycast golpea al jugador, ajusta la direcci�n de movimiento para evitar la colisi�n
                        directionToPlayer = ((Vector2)hit.point - (Vector2)transform.position).normalized;
                    }

                    // Calcula la velocidad de movimiento del enemigo
                    Vector2 moveVelocity = directionToPlayer * moveSpeed;

                    // Actualiza la velocidad del Rigidbody2D del enemigo para moverlo
                    rb.velocity = moveVelocity;

                    // Actualizamos el estado de la animaci�n
                    if (moveVelocity != Vector2.zero)
                    {
                        animator.SetBool("Camina", true);
                        if (isAttacking)
                        {
                            animator.ResetTrigger("Ataque");
                            isAttacking = false;
                        }
                    }
                    else
                    {
                        animator.SetBool("Camina", false);
                    }

                    // Flip del enemigo basado en la direcci�n del objetivo
                    if ((miraDerecha && target.position.x < transform.position.x) || (!miraDerecha && target.position.x > transform.position.x))
                    {
                        Flip();
                    }
                }
            }
            else
            {
                // Si el objetivo est� fuera del rango de detecci�n, el enemigo deber�a estar inactivo
                rb.velocity = Vector2.zero;
                animator.SetBool("Camina", false);
                if (isAttacking)
                {
                    animator.ResetTrigger("Ataque");
                    isAttacking = false;
                }
            }
        }
        else
        {
            // Si no hay objetivo, el enemigo deber�a estar inactivo
            rb.velocity = Vector2.zero;
            animator.SetBool("Camina", false);
            if (isAttacking)
            {
                animator.ResetTrigger("Ataque");
                isAttacking = false;
            }
        }
    }

    private void Flip()
    {
        miraDerecha = !miraDerecha;
        Vector3 localScale = transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;
    }
}